#ifndef C_PLAYERINPUT
#define C_PLAYERINPUT

#include "C_ActorMover.hpp"

class C_PlayerInput : public Component
{
    public:
		C_PlayerInput();
		~C_PlayerInput();
		void Initialize() override;
		void Update([[maybe_unused]] float deltaTime) override;
	private:
		C_ActorMover* actorMover;
};

#endif