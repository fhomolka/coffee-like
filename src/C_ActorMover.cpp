#include "C_ActorMover.hpp"

C_ActorMover::C_ActorMover(){}
C_ActorMover::~C_ActorMover(){}

void C_ActorMover::Initialize() {
    actorTransform = owner->GetComponent<C_Transform>();
}

void C_ActorMover::Update([[maybe_unused]] float deltaTime) {
    actorTransform->Translate(moveDir * 128);
    moveDir = CB::Vec2::ZERO;
}

void C_ActorMover::SetMoveDir(CB::Vec2 dir) {
    moveDir = dir;
}