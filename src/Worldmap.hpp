#ifndef CL_WORLDMAP_HPP
#define CL_WORLDMAP_HPP

#include <vector>
#include "C_ActorStats.hpp"
#include "Components/C_Transform.hpp"

//temp
#include <map>


static const int WORLD_AXIS_SIZE = 64;
static const int WORLD_SIZE = WORLD_AXIS_SIZE * WORLD_AXIS_SIZE;
static const int MAX_ACTORS = 255;


static const std::map<int, std::string> TERRAIN_MAP = {
	{0, " "},
	{1, "."}, //Ground
	{2, "#"}  //Wall
};


class Worldmap
{
	public:
		std::vector<std::vector<int>> Tiles;
		//std::string Renderable_Tiles[WORLD_AXIS_SIZE][WORLD_AXIS_SIZE];
		std::vector<std::vector<std::string>> Renderable_Tiles;
		std::vector<C_ActorStats*> Actors;
		int actorCount = 0;

		Worldmap(){};
		~Worldmap(){};

		void LoadMap()
		{
			this->Tiles = this->temp_testMap();
		}

		bool AddActor(C_ActorStats* newActor)
		{
			if (this->actorCount >= MAX_ACTORS)
			{
				Game::logger->LogError("[Worldmap | AddActor] Worldmap already has the max number of actors!");
				return false;
			}

			if (!newActor)
			{
				Game::logger->LogError("[Worldmap | AddActor] the new actor is a nullptr!");
				return false;
			}

			Game::logger->Log("[Worldmap | AddActor] Adding new actor" + newActor->owner->name);
			this->Actors.emplace_back(newActor);

			return true;
		}

		bool RemoveActor(C_ActorStats* remActor)
		{
			if (this->actorCount <= 0)
			{
				Game::logger->LogError("[Worldmap | RemoveActor] Worldmap already has no actors!");
				return false;
			}

			if (!remActor)
			{
				Game::logger->LogError("[Worldmap | RemoveActor] the actor to be removed is a nullptr!");
				return false;
			}

			std::vector<C_ActorStats*>::iterator iter = std::find(Actors.begin(), Actors.end(), remActor);

			if (iter == Actors.end())
			{
				Game::logger->LogError("[Worldmap | RemoveActor] Worldmap does not have actor " + remActor->GetSymbol() +"!");
				return false;
			}

			Game::logger->Log("[Worldmap | RemoveActor] Removing actor" + remActor->owner->name);
			this->Actors.erase(iter);

			return true;
		}

		std::vector<std::vector<std::string>> GetMap()
		{

			//fhomolka 05/08/2021 22:13 -> There has to be a nicer way
			Renderable_Tiles.clear();

			//Copy Map to a string format
			for (int y = 0; y < WORLD_AXIS_SIZE; ++y)
			{
				std::vector<std::string> tempMap;
				for (int x = 0; x < WORLD_AXIS_SIZE; ++x)
				{
					//Game::logger->Log("grabbing int value of tile");
					int tileInt = Tiles[y][x];
					std::string tempChar = TERRAIN_MAP.at(tileInt);
					tempMap.emplace_back(tempChar);
				}
				Renderable_Tiles.emplace_back(tempMap);
			}

			// Put actor symbols on the map
			for (C_ActorStats* actor : this->Actors)
			{
				CB::Vec2 actorPos = actor->owner->GetComponent<C_Transform>()->position;
				//Renderable_Tiles[actor.Position.x][actor.position.y] = actor.symbol;
				Renderable_Tiles[actorPos.x][actorPos.y] = " ";
			}

			return this->Renderable_Tiles;
		}

		std::vector<std::vector<int>> temp_testMap()
		{
			std::vector<std::vector<int>> testMap;
			for (int y = 0; y < WORLD_AXIS_SIZE; ++y)
			{
				std::vector<int> tempMap;
				for (int y = 0; y < WORLD_AXIS_SIZE; ++y)
				{
					tempMap.emplace_back(2);
				}
			testMap.emplace_back(tempMap);
			}

			return testMap;
		}
};

#endif