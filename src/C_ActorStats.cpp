#include "C_ActorStats.hpp"

C_ActorStats::C_ActorStats(int startingHP, int startingSP, int startingMP)
{
    this->health = startingHP;
    this->stamina = startingSP;
    this->mana = startingMP;
}

C_ActorStats::~C_ActorStats(){}

void C_ActorStats::Initialize()
{
    if (!owner->HasComponent<C_Transform>())
    {
        owner->AddComponent<C_Transform>(0, 0, 10, 10);
    }

    this->actorTransform = owner->GetComponent<C_Transform>();
};

void C_ActorStats::Update([[maybe_unused]] float deltaTime){}
void C_ActorStats::Render(){}

void C_ActorStats::TakeDamage(int amount)
{
    this->health -= amount;
}

int C_ActorStats::GetHealth() const
{
    return this->health;
}

int C_ActorStats::GetStamina() const
{
    return this->stamina;
}

int C_ActorStats::GetMana() const
{
    return this->mana;
}

bool C_ActorStats::IsAlive() const
{
    return this->health > 0;
}

CB::Vec2 C_ActorStats::GetPosition() const
{
    return this->actorTransform->position;
}

std::string C_ActorStats::GetSymbol() const
{
    return this->symbol;
}