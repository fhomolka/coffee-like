#ifndef C_ACTORSTATS
#define C_ACTORSTATS

#include "Component.hpp"
#include "Components/C_Transform.hpp"

#define DEFAULT_STARTING_VALUE 100

class C_ActorStats : public Component
{
	public:
		C_ActorStats(int startingHP = DEFAULT_STARTING_VALUE, int startingSP = DEFAULT_STARTING_VALUE, int startingMP = DEFAULT_STARTING_VALUE);
		~C_ActorStats();

		void Initialize() override;
		void Update([[maybe_unused]] float deltaTime) override;
		void Render() override;
		
		void TakeDamage(int amount);
		
		int GetHealth() const;
		int GetStamina() const;
		int GetMana() const;

		bool IsAlive() const;
   
   		CB::Vec2 GetPosition() const;
   		std::string GetSymbol() const;

	private:

		int health;
		int stamina;
		int mana;

		std::string symbol;

		C_Transform* actorTransform;
};

#endif//C_ACTORSTATS
