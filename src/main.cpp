#include "CoffeeBean.hpp"
#include "GameManager.hpp"

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;
	Game game = Game("Decaff");
	GameManager gameMan = GameManager();
	
	game.Initialize(1280, 720);

	gameMan.MakeEntities();

	game.logger->Log("Starting Main Loop");
	game.MainLoop();

	game.logger->Log("Freeing game memory");
	return 0;
}

