#include "C_PlayerInput.hpp"

std::string moveUpKeyCode = "moveUp";
std::string moveDownKeyCode = "moveDown";
std::string moveLeftKeyCode = "moveLeft";
std::string moveRightKeyCode = "moveRight";

C_PlayerInput::C_PlayerInput(){}
C_PlayerInput::~C_PlayerInput(){}

void C_PlayerInput::Initialize() {
    actorMover = owner->GetComponent<C_ActorMover>();
    Game::inputManager->RegisterKey(moveUpKeyCode, "w");
    Game::inputManager->RegisterKey(moveDownKeyCode, "s");
    Game::inputManager->RegisterKey(moveLeftKeyCode, "a");
    Game::inputManager->RegisterKey(moveRightKeyCode, "d");
}

void C_PlayerInput::Update([[maybe_unused]] float deltaTime) {
    CB::Vec2 moveDir = CB::Vec2::ZERO;
    if (Game::inputManager->IsKeyDown(moveUpKeyCode))
	{
		moveDir.y -= 1.0f;
	}
	else if(Game::inputManager->IsKeyDown(moveDownKeyCode))
	{
		moveDir.y += 1.0f;
	}
	
    if (Game::inputManager->IsKeyDown(moveLeftKeyCode))
	{
		moveDir.x -= 1.0f;
	}
	else if(Game::inputManager->IsKeyDown(moveRightKeyCode))
	{
		moveDir.x += 1.0f;
	}

    actorMover->SetMoveDir(moveDir);
}