#include "GameManager.hpp"
#include "Components/C_Transform.hpp"
#include "Components/C_Text.hpp"
#include "Components/C_Tile.hpp"
#include "Components/C_Sprite.hpp"
#include "C_ActorStats.hpp"
#include "C_PlayerInput.hpp"
#include "Util.hpp"

GameManager::GameManager(){}

GameManager::~GameManager(){}

const int tileSize = 32;
const char* ACTOR_FONT_ID = "kong";
bool GameManager::MakeEntities()
{
	Game::assetManager->LoadFromAssetsJson("./assets/assets.json");

	const std::string tileMapId = "scribbletile";
	MakeTile(CB::Vec2{0,0}, tileMapId, CB::Vec2{0,0});
	MakeTile(CB::Vec2{1 * tileSize, 0 * tileSize}, tileMapId, CB::Vec2{1,0});

	Game::logger->Log("Making Player!");
	Entity& tplayer = Game::entityManager->AddEntity("Player");
	this->player = &tplayer;
	this->player->AddComponent<C_Transform>(CB::Vec2{0, 0}, tileSize, tileSize);
	player->AddComponent<C_Tile>(tileMapId, CB::Vec2{2, 8});
	this->player->AddComponent<C_ActorStats>();
	player->AddComponent<C_ActorMover>();
	player->AddComponent<C_PlayerInput>();
	
	return true;
}


Entity& GameManager::MakeActor(std::string name, std::string symbol, CB::Vec2 position)
{
	Entity& newActor = Game::entityManager->AddEntity(name);
	newActor.AddComponent<C_Transform>(position, 10, 10);
	newActor.AddComponent<C_Text>(position, symbol.c_str(), ACTOR_FONT_ID, CB::Colour::WHITE);
	newActor.AddComponent<C_ActorStats>();

	return newActor;
}

Entity& GameManager::MakeTile(CB::Vec2 position, std::string tilemap, CB::Vec2 tileCoords)
{
	int id = IntId::Vec2IntId(tileCoords, 720);

	Entity& newTile = Game::entityManager->AddEntity(std::to_string(id));
	newTile.AddComponent<C_Transform>(position, tileSize, tileSize);
	newTile.AddComponent<C_Tile>(tilemap, tileCoords);
	return newTile;
}