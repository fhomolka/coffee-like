#ifndef CL_GAMEMAN
#define CL_GAMEMAN

#include "Game.hpp"
#include "Entity.hpp"
#include "Types/Vector2.hpp"
#include "Worldmap.hpp"

class GameManager
{
   public:
      GameManager();
      ~GameManager();

      bool MakeEntities();

      Entity& MakeActor(std::string name, std::string symbol, CB::Vec2 position);
      Entity& MakeTile(CB::Vec2 position, std::string tilemap, CB::Vec2 tileCoords);
   private:
      Entity* player;
};

#endif//CL_GAMEMAN
