#ifndef C_ACTORMOVER
#define C_ACTORMOVER

#include "Component.hpp"
#include "Components/C_Transform.hpp"

class C_ActorMover : public Component
{
    public:
		C_ActorMover();
		~C_ActorMover();
		void Initialize() override;
		void Update([[maybe_unused]] float deltaTime) override;
		void SetMoveDir(CB::Vec2 dir);
	private:
		C_Transform* actorTransform;
        CB::Vec2 moveDir;
};

#endif