 workspace "Coffee-Like"
 	architecture "x64"

 	configurations
    {
        "Debug",
        "Release"
    }

include "thirdparty/coffeebean/premake5.lua"

project "CoffeeLike"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++20"

	targetdir ("bin/%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}/%{prj.name}")
	objdir ("obj/%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}/%{prj.name}")

	files
	{
		"src/**.cpp",
	}

	includedirs
	{
		"src",
		"thirdparty/coffeebean/include",
		"thirdparty/coffeebean/thirdparty/json/single_include",
		"thirdparty/coffeebean/thirdparty/Xoshiro/"
	}

	libdirs
    {
    	"thirdparty/coffeebean/bin/%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}/CoffeeBean"
    }

    links
	{
		"CoffeeBean",
		"SDL2",
		"SDL2_image",
		"SDL2_ttf",
		"SDL2_mixer"

	}

	buildoptions
	{
		"-Wfatal-errors",
		"-Wno-narrowing"
	}

	filter "system:windows"
		includedirs
		{
			"thirdparty/SDL2/include",
		}

     	links
     	{
     		"mingw32",
     		"SDL2main"
     	}

    filter "configurations:Debug"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        runtime "Release"
        optimize "on"


    postbuildcommands
    {
    	"{COPY} %{cfg.targetdir}/%{prj.name} ./%{prj.name}"
    }
