#!/usr/bin/bash

mkdir -pv build
cmake -S . -B build
cd build
make
cd ..
